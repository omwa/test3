package com;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="employee")
public class Employee {
	public int getEMPLOYEE_ID() {
		return EMPLOYEE_ID;
	}
	public void setEMPLOYEE_ID(int eMPLOYEE_ID) {
		EMPLOYEE_ID = eMPLOYEE_ID;
	}
	public String getEMPLOYEE_NAME() {
		return EMPLOYEE_NAME;
	}
	public void setEMPLOYEE_NAME(String eMPLOYEE_NAME) {
		EMPLOYEE_NAME = eMPLOYEE_NAME;
	}
	public String getEMPLOYEE_ADD() {
		return EMPLOYEE_ADD;
	}
	public void setEMPLOYEE_ADD(String eMPLOYEE_ADD) {
		EMPLOYEE_ADD = eMPLOYEE_ADD;
	}
	public String getJOINING_DATE() {
		return JOINING_DATE;
	}
	public void setJOINING_DATE(String jOINING_DATE) {
		JOINING_DATE = jOINING_DATE;
	}
	/*public Set<RegisterUser> getUser() {
		return user;
	}
	public void setUser(Set<RegisterUser> user) {
		this.user = user;
	}*/
	@Id
	@Column(name="EMPLOYEE_ID")
private int EMPLOYEE_ID;
	@Column(name="EMPLOYEE_NAME")
private String EMPLOYEE_NAME;
	@Column(name="EMPLOYEE_ADD")
private String EMPLOYEE_ADD;
	@Column(name="JOINING_DATE")
private String JOINING_DATE;
	//@Column(name="EMPLOYEE_EMAIL")
/*private String EMPLOYEE_EMAIL;
	@Column(name="EMPLOYEE_MOBILENO")
private int EMPLOYEE_MOBILENO; 
	@Column(name="BIRTH_DATE")
	private String birth_date;
	
	@Column(name="GENDER")
	private String gender;*/
/*@OneToMany(targetEntity=RegisterUser.class,cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	@JoinColumn(name="MEMBER_ID",referencedColumnName="MEMBER_ID")
	private Set<RegisterUser>user;*/
	@ManyToOne(cascade=CascadeType.ALL)

	private	RegisterUser user;
	public RegisterUser getUser() {
		return user;
	}
	public void setUser(RegisterUser user) {
		this.user = user;
	}
	
}
