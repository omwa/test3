package com;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="loginmaster")
public class login {

	public int getLoginid() {
		return loginid;
	}
	public void setLoginid(int loginid) {
		this.loginid = loginid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	/*public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	public RegisterUser getUser() {
		return user;
	}
	public void setUser(RegisterUser user) {
		this.user = user;
	}*/
	@Id
	@Column(name="LOGINID")
private int loginid;
	public RegisterUser getUser() {
		return user;
	}
	public void setUser(RegisterUser user) {
		this.user = user;
	}
	@Column(name="USERNAME")
private String username;
	@Column(name="PASSWORD")
private String password;
	@Column(name="TYPE")
	private String type;
	/*@OneToOne(targetEntity=RegisterUser.class,cascade=CascadeType.ALL)
	@JoinColumn(name="EMPLOYEE_ID",referencedColumnName="EMPLOYEE_ID")
private Employee employee;*/
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="MEMBER_ID",referencedColumnName="MEMBER_ID")
private RegisterUser user;
	
}
